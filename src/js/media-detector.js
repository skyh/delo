function detectCssMedia() {
	var detector = document.getElementById('media-detector');
    var tag = window.getComputedStyle(detector, null).getPropertyValue('content');
    tag = tag.replace(/"/g, '');
    
    return tag;
}
