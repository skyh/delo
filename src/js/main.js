function initShareBlock(i, block) {
    var quickServices = ['vkontakte', 'facebook', 'twitter', 'moimir', 'odnoklassniki'];
    new Ya.share({
        l10n: 'ru',
        element: block,
        theme: 'counter',
        elementStyle: {
            'type': 'icon',
            'border': false,
            'quickServices': quickServices
        },
        link: 'http://delo.navalny.ru/',
        title: 'Дело «Ив Роше». Ущерб, которого не было.',
        description: 'Навального заставили молчать, поэтому о том, что дело «Ив Роше» сфабриковано, расскажу я.',
        image: 'http://delo.navalny.ru/img/share.png',
        serviceSpecific: {
            twitter: {
                title: 'Навального заставили молчать, поэтому о том, что дело «Ив Роше» сфабриковано, расскажу я. pic.twitter.com/wnewbDY8IV'
           }
        },
        onshare: function(social_network) {
            dataLayer.push({
                'social_network': social_network,
                'event': 'share'
            });
        }
    });
}

function highlightMenu(href) {
    var activeClass = 'menu_link_active';
    var disabledLinks = $('#menu a');
    var enabledLink = $('#menu a[href="' + href + '"]');
    
    disabledLinks.removeClass(activeClass);
    enabledLink.addClass(activeClass);
}

function isElementInViewport(el) {
    var rect = el.getBoundingClientRect();

    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
        rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
    );
}

$(function() {
    $('.share_block').each(initShareBlock);
    $('#menu').sticky();
    $(window).on('hashchange', function(event) {
        highlightMenu(document.location.hash);
    });
    
    $('#speech-switch').click(function(e) {
        var activeClass = 'speech_switch_active';
        var target = e.target;
        var targetId = target.id;

        switch (targetId) {
            case 'alexey-speech-switch':
                $('#oleg-speech').hide();
                $('#alexey-speech').show();
                $('#alexey-speech-switch').addClass(activeClass);
                $('#oleg-speech-switch').removeClass(activeClass);
                break;
            case 'oleg-speech-switch':
                $('#alexey-speech').hide();
                $('#oleg-speech').show();
                $('#oleg-speech-switch').addClass(activeClass);
                $('#alexey-speech-switch').removeClass(activeClass);
                break;
        }
    });
});
